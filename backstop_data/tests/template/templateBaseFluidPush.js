const templateUrl = 'templates/base/examples/fluid-push.html'

module.exports = options => {
  return {
    scenarios: [
      {
        label: 'Base Fluid Push',
        selectors: ['.template-base'],
        url: `${options.baseUrl}/${templateUrl}`
      },
      {
        label: 'Base Fluid Push - header',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['header.br-header']
      },
      {
        label: 'Base Fluid Push - footer',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.br-footer'],
        removeSelectors: ['.br-header'],
        scrollToSelector: '.br-footer'
      },
      {
        label: 'Base Fluid Push - menu',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.template-base'],
        clickSelector: '.br-button#navigation',
      },
      {
        label: 'Base Fluid Push - breadcrumb',
        url: `${options.baseUrl}/${templateUrl}`,
        removeSelectors: ['.br-header'],
        selectors: ['.br-breadcrumb']
      }
    ]
  }
}
