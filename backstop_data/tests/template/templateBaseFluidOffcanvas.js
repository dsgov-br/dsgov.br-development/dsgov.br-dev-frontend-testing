const templateUrl = 'templates/base/examples/fluid-offcanvas.html'

module.exports = options => {
  return {
    scenarios: [
      {
        label: 'Base Fluid Offcanvas',
        selectors: ['.template-base'],
        url: `${options.baseUrl}/${templateUrl}`
      },
      {
        label: 'Base Fluid Offcanvas - header',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['header.br-header']
      },
      {
        label: 'Base Fluid Offcanvas - footer',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.br-footer'],
        removeSelectors: ['.br-header'],
        scrollToSelector: '.br-footer'
      },
      {
        label: 'Base Fluid Offcanvas - menu',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.template-base'],
        clickSelector: '.br-button#navigation',
      },
      {
        label: 'Base Fluid Offcanvas - breadcrumb',
        url: `${options.baseUrl}/${templateUrl}`,
        removeSelectors: ['.br-header'],
        selectors: ['.br-breadcrumb']
      }
    ]
  }
}
