const component = "checkbox";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
      //   {
      //     label: `${component} - Click`,
      //     label: `${component}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     clickSelector: ".br-checkbox input[type='checkbox']",
      //   },
      //   {
      //     label: `${component} - Hover`,
      //     label: `${component}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     hoverSelector: ".br-checkbox input[type='checkbox']",
      //   },
      //   {
      //     label: `${component} - Message`,
      //     label: `${component}`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     hoverSelector: ".feedback",
      //   },
    ],
  };
};
