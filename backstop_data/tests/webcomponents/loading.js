const component = "loading";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
        delay: 5000,
        removeSelectors: [
          ".is-loading[md][label], .loading[md][label], [loading][md][label]",
          ".is-loading[label], .loading[label], [loading][label]",
        ],
      },
      //   {
      //     label: `${component} - With Percentage`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     selectors: [".br-card > .front"],
      //     delay: 5000,
      //   },
    ],
  };
};
