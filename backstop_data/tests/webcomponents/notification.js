const component = "notification";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
      //   {
      //     label: `${component} - ClickMenuFirstItem`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     clickSelector: [".br-tab .tab-nav ul .tab-item:first-child"],
      //     postInteractionWait: 2000,
      //   },
      //   {
      //     label: `${component} - ClickMenuLastItem`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     clickSelector: [".br-tab .tab-nav ul .tab-item:last-child"],
      //     postInteractionWait: 2000,
      //   },
      //   {
      //     label: `${component} - ClickOnListItem`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     clickSelector: [".br-tab, .br-list, button.br-item"],
      //     postInteractionWait: 2000,
      //   },
    ],
  };
};
