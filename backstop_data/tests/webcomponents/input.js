const component = "input";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component} referências`,
        url: `${options.baseUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
      // {
      //   label: `${component} padrão`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input"],
      // },
      // {
      //   label: `${component} hover`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   hoverSelector: ".br-input #input-default",
      //   selectors: [".br-input"],
      // },
      // {
      //   label: `${component} focus`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   clickSelector: ".br-input #input-default",
      //   selectors: [".br-input"],
      // },
      // {
      //   label: `${component} alta densidade`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.small"],
      // },
      // {
      //   label: `${component} baixa densidade`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.large"],
      // },
      // {
      //   label: `${component} com icone`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input #input-icon"],
      // },
      // {
      //   label: `${component} com botao`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.input-button"],
      // },
      // {
      //   label: `${component} com rotulo lateral`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.input-inline"],
      // },
      // {
      //   label: `${component} destaque`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.input-highlight"],
      // },
      // {
      //   label: `${component} success`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.success"],
      // },
      // {
      //   label: `${component} danger`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.danger"],
      // },
      // {
      //   label: `${component}warning`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.warning"],
      // },
      // {
      //   label: `${component} info`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".br-input.info"],
      // },
      // {
      //   label: `${component} em fundo escuro`,
      //   url: `${options.baseUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: [".bg-primary-darken-02"],
      // },
    ],
  };
};
