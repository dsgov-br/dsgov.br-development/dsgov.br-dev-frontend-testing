const component = "button";
// const selectorClick = `document.querySelector("#br-button > div > br-button:nth-child(3)").shadowRoot.querySelector("button.br-button.secondary")`;

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
        // hideSelectors: ["#loading-01"],
      },
      // {
      //   label: `${component} - Primary`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: ["#primary-01"],
      //   // readySelector: "#primary-01",
      //   // delay: 2000,
      // },
      // {
      //   label: `${component} - Click`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   clickSelector: ["#primary-01"],
      //   // postIteractionWait: 2000,
      //   // hideSelectors: [".loading"],
      // },
      // {
      //   label: `${component} - Hover`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   hoverSelector: ".br-button.secondary",
      //   hideSelectors: [".loading"],
      // },
      // {
      //   label: `${component} - ClickCircleButton`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   clickSelector: ".br-button.secondary.circle.small",
      //   hideSelectors: [".loading"],
      // },
      // {
      //   label: `${component} - HoverCircleButton`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   clickSelector: ".br-button.secondary.circle.small",
      //   hideSelectors: [".loading"],
      // },
    ],
  };
};
