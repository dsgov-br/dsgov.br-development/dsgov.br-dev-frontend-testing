const component = "avatar";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
    ],
  };
};
