const component = "menu";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
      //   {
      //     label: `${component}- Click`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     clickSelector: ".support:last-child",
      //   },
    ],
  };
};
