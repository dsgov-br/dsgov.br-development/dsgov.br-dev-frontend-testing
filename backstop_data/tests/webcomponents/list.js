const component = "list";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
      // {
      //   label: `${component} - Click Collapsible`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   selectors: "viewport",
      //   scrollToSelector: ".br-list .br-item > .content",
      //   clickSelector: ".br-list .br-item > .content",
      //   postInteractionWait: 3000,
      // },
      // {
      //   label: `${component} - HoverCollapsibleList`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   scrollToSelector: ".br-list .br-item > .content",
      //   hoverSelector: ".br-list .br-item > .content",
      //   selectors: "viewport",
      //   postInteractionWait: 3000,
      // },
    ],
  };
};
