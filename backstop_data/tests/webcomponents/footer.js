const component = "footer";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
        requireSameDimensions: false,
      },
      //   {
      //     label: `${component} - Default`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     selectors: [".br-footer"],
      //     requireSameDimensions: false,
      //   },
      //   {
      //     label: `${component} - Inverted`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     selectors: [".br-footer.inverted"],
      //     scrollToSelector: ".br-footer.inverted",
      //     requireSameDimensions: false,
      //     misMatchThreshold: 7,
      //   },
    ],
  };
};
