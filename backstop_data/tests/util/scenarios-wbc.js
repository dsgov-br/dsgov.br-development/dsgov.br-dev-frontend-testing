module.exports = (options) => {
  const avatar = require("../webcomponents/avatar")(options).scenarios;
  const breadcrumb = require("../webcomponents/breadcrumb")(options).scenarios;
  const button = require("../webcomponents/button")(options).scenarios;
  const card = require("../webcomponents/card")(options).scenarios;
  const checkbox = require("../webcomponents/checkbox")(options).scenarios;
  const footer = require("../webcomponents/footer")(options).scenarios;
  const header = require("../webcomponents/header")(options).scenarios;
  const input = require("../webcomponents/input")(options).scenarios;
  const item = require("../webcomponents/item")(options).scenarios;
  const list = require("../webcomponents/list")(options).scenarios;
  const loading = require("../webcomponents/loading")(options).scenarios;
  const magicButton = require("../webcomponents/magic-button")(
    options
  ).scenarios;
  const menu = require("../webcomponents/menu")(options).scenarios;
  const message = require("../webcomponents/message")(options).scenarios;
  const notification = require("../webcomponents/notification")(
    options
  ).scenarios;
  const switch_wbc = require("../webcomponents/switch")(options).scenarios;
  const tab = require("../webcomponents/tab")(options).scenarios;

  return {
    scenarios: [
      ...avatar,
      ...breadcrumb,
      ...button,
      ...card,
      ...checkbox,
      ...footer,
      ...header,
      ...input,
      ...item,
      ...list,
      ...loading,
      ...magicButton,
      ...menu,
      ...message,
      ...notification,
      ...switch_wbc,
      ...tab,
    ],
  };
};
