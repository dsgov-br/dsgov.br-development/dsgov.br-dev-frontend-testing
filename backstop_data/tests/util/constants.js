//------------------
// URL DE AMBIENTES
//------------------

exports.PROD_URL = `https://www.gov.br/ds/`;
exports.HOM_URL = `https://hom-dsgov.estaleiro.serpro.gov.br`;
exports.LOCAL_URL = "http://127.0.0.1:4200";
exports.DEFAULT_URL = "https://dev-dsgov.estaleiro.serpro.gov.br";
exports.WBC_URL = "http://127.0.0.1:8080";

//-------------------------------------
// PROPRIEDADES CONSTANTES DE CENÁRIOS
//-------------------------------------

exports.DEFAULT_DELAY = 500;

exports.SCENARIO_DEFAULTS = {
  misMatchThreshold: 0.0001,
  delay: 250,
};

//----------------------
// VIEWPORTS DO PROJETO
// ---------------------

exports.VIEWPORTS = [
  {
    label: "Smartphone Portrait",
    width: 574,
    height: 760,
  },
  {
    label: "Smartphone Landscape",
    width: 991,
    height: 480,
  },
  {
    label: "Tablet Landscape",
    width: 1279,
    height: 768,
  },
  {
    label: "Desktop",
    width: 1440,
    height: 900,
  },
  {
    label: "TV",
    width: 1600,
    height: 900,
  },
  {
    label: "ZOOM 200%",
    width: 1440,
    height: 900,
  },
  {
    label: "ZOOM 300%",
    width: 1440,
    height: 900,
  },
];
