const componentUrl = "assets/design-system/dist/components/signin/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Signin",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            // {
            //     "label": "Signin - Click",
            //     "url": `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     clickSelectors:  [".br-sign-in", ".primary"],
            //     "postInteractionWait": 10000,
            // },
            // {
            //     "label": "Signin - Hover",
            //     "url": `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hoverSelectors: [".br-sign-in", ".secondary"]
            // },
            // {
            //     "label": "Signin - ClickCircleSignin",
            //     "url": `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     clickSelectors: [".br-sign-in", ".primary",".circle"]
            // }
        ]
    }
}