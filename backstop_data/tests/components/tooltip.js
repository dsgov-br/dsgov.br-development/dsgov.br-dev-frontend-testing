const componentUrl = "assets/design-system/dist/components/tooltip/examples.html";

module.exports = options => {
    return {
        scenarios: [
            {
                label: "Tooltip - Texto Simples",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hoverSelector: "img",
                hideSelectors: ["img"],
                postInteractionWait: 1000,
                readySelector: ".br-tooltip",
                misMatchThreshold: 2
            },
            // {
            //     label: "Tooltip - Texto Auxiliar Informação",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelectors: ["a.h5"],
            //     selectorExpansion: true,
            //     postInteractionWait: 5000,
            //     delay: 2000,
            //     readySelector: ".br-tooltip",
            //     misMatchThreshold: 2
            // },
            // {
            //     label: "Tooltip - Texto Auxiliar Sucesso",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelectors: [".mb-3:nth-child(2)"],
            //     selectorExpansion: true,
            //     postInteractionWait: 5000,
            //     delay: 2000,
            //     readySelector: ".br-tooltip",
            //     misMatchThreshold: 2
            // },
            // {
            //     label: "Tooltip - Texto Auxiliar Erro",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelectors: [".mb-3:nth-child(3)"],
            //     selectorExpansion: true,
            //     postInteractionWait: 5000,
            //     delay: 2000,
            //     readySelector: ".br-tooltip",
            //     misMatchThreshold: 2
            // },
            // {
            //     label: "Tooltip - Texto Auxiliar Advertência",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelectors: [".mb-3:nth-child(4)"],
            //     selectorExpansion: true,
            //     postInteractionWait: 5000,
            //     delay: 2000,
            //     readySelector: ".br-tooltip",
            //     misMatchThreshold: 2
            // },
            // {
            //     label: "Tooltip - Texto com Input",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelector: ".br-input .has-icon",
            //     postInteractionWait: 5000,
            //     delay: 2000,
            //     readySelector: ".br-tooltip",
            // },
            // {
            //     label: "Tooltip - PopOver Simples",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelector: ".row:nth-of-type(4) a.h5",
            //     postInteractionWait: 2000,
            //     readySelector: ".br-tooltip",
            // },
            // {
            //     label: "Tooltip - PopOver Warning",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelector: ".row:nth-of-type(5) a.h5",
            //     postInteractionWait: 2000,
            //     readySelector: ".br-tooltip",
            //     misMatchThreshold: 10
            // },
            // {
            //     label: "Tooltip - PopOver Erro",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelector: ".row:nth-of-type(6) a.h5",
            //     postInteractionWait: 2000,
            //     readySelector: ".br-tooltip",
            // },
            // {
            //     label: "Tooltip - PopOver Imagem",
            //     url: `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     hideSelectors: ["img"],
            //     hoverSelector: ".row:nth-of-type(7) a.h5",
            //     postInteractionWait: 2000,
            //     readySelector: ".br-tooltip",
            // }
        ]
    }
}