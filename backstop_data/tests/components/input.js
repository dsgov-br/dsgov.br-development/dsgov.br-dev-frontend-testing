const componentUrl = "assets/design-system/dist/components/input/examples.html";

module.exports = options => {
  
  return {
    "scenarios": [
      {
        "label": "Referencias",
        "url": `${options.baseUrl}/${componentUrl}`,
      },
      {
        "label": "Input padrao",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input"]
      },
      {
        "label": "Input hover",
        "url": `${options.baseUrl}/${componentUrl}`,
        "hoverSelector": ".br-input #input-default",
        "selectors": [".br-input"]
      },
      {
        "label": "Input focus",
        "url": `${options.baseUrl}/${componentUrl}`,
        "clickSelector": ".br-input #input-default",
        "selectors": [".br-input"]
      },
      {
        "label": "Input alta densidade",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.small"]
      },
      {
        "label": "Input baixa densidade",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.large"]
      },
      {
        "label": "Input com icone",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input #input-icon"]
      },
      {
        "label": "Input com botao",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.input-button"]
      },
      {
        "label": "Input com rotulo lateral",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.input-inline"]
      },
      {
        "label": "Input destaque",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.input-highlight"]
      },
      {
        "label": "Input success",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.success"]
      },
      {
        "label": "Input danger",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.danger"]
      },
      {
        "label": "Input warning",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.warning"]
      },
      {
        "label": "Input info",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".br-input.info"]
      },
      {
        "label": "Input em fundo escuro",
        "url": `${options.baseUrl}/${componentUrl}`,
        "selectors": [".bg-primary-darken-02"]
      },
    ]
  }
}