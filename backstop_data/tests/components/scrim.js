const componentUrl = "assets/design-system/dist/components/scrim/examples.html";

module.exports = options => {
    return {
        scenarios: [{
                label: "Scrim",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hideSelectors: ["img"]
            },
            {
                label: "Scrim - ClickTestarEfeito",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hideSelectors: ["img"],
                clickSelector: ".scrimexemplo > .br-button.primary"
            }
        ]
    }
}