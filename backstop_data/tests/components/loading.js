const componentUrl = "assets/design-system/dist/components/loading/examples.html";

module.exports = options => {
    return {
        "scenarios": [
            {
                label: "Loading",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                delay: 5000,
                removeSelectors: [".is-loading[md][label], .loading[md][label], [loading][md][label]",
                                ".is-loading[label], .loading[label], [loading][label]"]
            },
            {
                label: "Loading - With Percentage",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                selectors: [".br-card > .front"],
                delay: 5000
            }
        ]
    }
}