const componentUrl = "assets/design-system/dist/components/badge/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
            "label": "Badge",
            "url": `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
        }]
    }
}