const componentUrl = "assets/design-system/dist/components/card/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Card",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            
            // {
            //     "label": "Card - Click on Expand",
            //     "url": `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     "scrollToSelector": ".br-button",
            //     "clickSelector": "[data-expanded]"
            // }
        ]
    }
}