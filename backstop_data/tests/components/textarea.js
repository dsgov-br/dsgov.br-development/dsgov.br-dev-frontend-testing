const componentUrl = "assets/design-system/dist/components/textarea/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                label: "Textarea",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            {
                label: "Textarea - Click",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                clickSelector: ".br-textarea textarea"
            }
        ]
    }
}