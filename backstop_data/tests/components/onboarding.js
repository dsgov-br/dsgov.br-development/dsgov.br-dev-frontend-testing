const { option } = require("yargs")

const componentUrl = 'assets/design-system/dist/pages/onboarding/examples.html'

module.exports = options => {
  return {
    scenarios: [
      {
        label: 'Onboarding',
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`
      },
      {
        label: 'Onboarding - Enfase na Interface',
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: 'button#intro-interface',
        postInteractionWait: 2000
      },
      {
        label: 'Onboarding - Enfase Textual',
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: 'button#intro-textual',
        postInteractionWait: 2000
      }, 
      {
        label: 'Onboarding - Botão Pular Tutorial',
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: ['button#intro-interface', 'a.introjs-skipbutton'],
        postInteractionWait: 1000,
      },
      {
        label: 'Onboarding - Botão Próximo',
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: ['button#intro-textual', '.introjs-button.introjs-nextbutton'],
        postInteractionWait: 1000,
      },
      {
        label: 'Onboarding - Botão Anterior',
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: ['button#intro-textual', '.introjs-button.introjs-prevbutton'],
        postInteractionWait: 1000,
      }
    ]
  }
}
