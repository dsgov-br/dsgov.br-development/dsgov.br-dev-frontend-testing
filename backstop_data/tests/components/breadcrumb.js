const componentUrl =
  "assets/design-system/dist/components/breadcrumb/examples.html";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: "Breadcrumb",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        readySelector: ".crumb[data-active]",
      },
      {
        label: "Breadcrumb - ClickFirstChild",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: ".crumb-list > :first-child",
        postInteractionWait: 2000,
        viewports: [
          {
            label: "Smartphone Landscape",
            width: 991,
            height: 480,
          },
          {
            label: "Tablet Landscape",
            width: 1279,
            height: 768,
          },
          {
            label: "Desktop",
            width: 1440,
            height: 900,
          },
          {
            label: "TV",
            width: 1600,
            height: 900,
          },
          {
            label: "ZOOM 200%",
            width: 1440,
            height: 900,
          },
        ],
      },
      {
        label: "Breadcrumb - HoverFirstChild",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        hoverSelector: ".crumb-list > :first-child",
        viewports: [
          {
            label: "Smartphone Landscape",
            width: 991,
            height: 480,
          },
          {
            label: "Tablet Landscape",
            width: 1279,
            height: 768,
          },
          {
            label: "Desktop",
            width: 1440,
            height: 900,
          },
          {
            label: "TV",
            width: 1600,
            height: 900,
          },
          {
            label: "ZOOM 200%",
            width: 1440,
            height: 900,
          },
        ],
      },

      {
        label: "Breadcrumb - Clickdropdown",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: ".crumb-list .crumb.menu-mobil",
        viewports: [
          {
            label: "Smartphone Portrait",
            width: 574,
            height: 760,
          },
          {
            label: "ZOOM 200%",
            width: 1440,
            height: 900,
          },
          {
            label: "ZOOM 300%",
            width: 1440,
            height: 900,
          },
        ],
      },
    ],
  };
};
