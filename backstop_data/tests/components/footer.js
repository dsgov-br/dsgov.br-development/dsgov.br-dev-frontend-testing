const componentUrl = "assets/design-system/dist/components/footer/examples.html";

module.exports = (options) => {
    return {
        "scenarios": [{
                "label": "Footer",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                requireSameDimensions: false,
            },
            {
                "label": "Footer - Default",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "selectors": [".br-footer"],
                requireSameDimensions: false,
            },
            {
                "label": "Footer - Inverted",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "selectors": [".br-footer.inverted"],
                "scrollToSelector": ".br-footer.inverted",
                requireSameDimensions: false,
                misMatchThreshold: 7
            }
        ]
    }
}