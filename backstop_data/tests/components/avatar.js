const componentUrl = "assets/design-system/dist/components/avatar/examples.html";

module.exports = options => {
    return {
        scenarios: [{
            "label": "Avatar",
            "url": `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
        }]
    }
}