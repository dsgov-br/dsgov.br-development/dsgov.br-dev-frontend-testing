const componentUrl = "assets/design-system/dist/components/wizard/examples.html";

module.exports = options => {
    return {
        "scenarios": [
            {
                label: "Wizard",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                postInteractionWait: 5000,
                misMatchThreshold : 6.10,
                delay: 5000
            },
            {
                label: "Wizard Vertical - Click Last Child",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                selectors: [".br-wizard[vertical]"],
                clickSelector: ".br-wizard[vertical] .wizard-progress .wizard-progress-btn:last-child",
                postInteractionWait: 2000
            },
            {
                label: "Wizard Vertical - Click Button Next",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                selectors: [".br-wizard[vertical]"],
                clickSelector: ".wizard-btn-next",
                postInteractionWait: 2000
            },
            {
                label: "Wizard Horizontal - Click Last Child",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                selectors: [".br-wizard:not([vertical])"],
                clickSelector: ".br-wizard:not([vertical]) .wizard-progress .wizard-progress-btn:last-child",
                postInteractionWait: 2000
            },
            {
                label: "Wizard Horizontal - Click Button Next",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                selectors: [".br-wizard:not([vertical])"],
                clickSelector: ".br-wizard:not([vertical]) .wizard-btn-next",
                postInteractionWait: 2000
            }
        ]
    }
}