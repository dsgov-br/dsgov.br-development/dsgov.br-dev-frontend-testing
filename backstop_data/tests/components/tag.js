const componentUrl = "assets/design-system/dist/components/tag/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                label: "Tag",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            {
                label: "Tag - Click on First tag",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                clickSelector: ".br-tag"
            },
            {
                label: "Tag - Click Close Button",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                clickSelector: ".br-tag > .close"
            },
            {
                label: "Tag - Click Tag Seleção",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                clickSelector: ".br-tag.interaction-select"
            },
           
        ]
    }
}