let designSystem = [
  "/",
  "introducao/como-comecar",
  "introducao/principios",
  "introducao/sobre",
  "introducao/prototipando",
  "fundamentos-visuais/cores",
  "fundamentos-visuais/iconografia",
  "fundamentos-visuais/espacamento",
  "fundamentos-visuais/estados",
  "fundamentos-visuais/grid",
  "fundamentos-visuais/superficie",
  "fundamentos-visuais/tipografia",
  "guias/manuais-orientadores",
  "guias/boas-praticas-de-css",
  "guias/boas-praticas-de-html",
  "guias/acessibilidade-no-codigo-html",
  "guias/codificacao-javascript",
  "guias/uso-do-wai-aria",
  "guias/utilitarios-css",
  "guias/navegadores-suportados",
  "guias/construcao-de-componentes",
  "padroes/onboarding",
  "padroes/formulario",
  "components/status",
  "templates/base",
  "release-notes/release-notes",
  "legislacao",
  "downloads",
];

let components = [
  "components/accordion",
  "components/avatar",
  "components/badge",
  "components/breadcrumb",
  "components/button",
  "components/card",
  "components/checkbox",
  "components/datepicker",
  "components/divider",
  "components/footer",
  "components/header",
  "components/input",
  "components/item",
  "components/list",
  "components/loading",
  "components/menu",
  "components/message",
  "components/modal",
  "components/notification",
  "components/pagination",
  "components/radio",
  "components/scrim",
  "components/select",
  "components/skiplink",
  "components/table",
  "components/tab",
  "components/tag",
  "components/textarea",
  "components/tooltip",
  "components/upload",
  "components/wizard",
];

let pages = ["pages/onboarding"];

let all = [];

module.exports = (options) => {
  designSystem.forEach((page) => {
    all.push({
      label: `GOVBR - ${page}`,
      url: `${options.baseUrl}/${page}`,
      referenceUrl: `${options.baseUrl}/${page}`,
      removeSelectors: [
        "#barra-brasil",
        ".access-button",
        "__web-inspector-hide-shortcut__",
        ".background-color-primary-default",
        "br-header",
        "br-menu",
      ],
      mergeImgHack: true,
    });
  });

  components.forEach((page) => {
    all.push(
      {
        label: `GOVBR - DESIGNER - ${page}`,
        url: `${options.baseUrl}/${page}/?tab=designer`,
        referenceUrl: `${options.baseUrl}/${page}/?tab=designer`,
        delay: 3000,
        removeSelectors: [
          "#barra-brasil",
          ".access-button",
          ".background-color-primary-default",
          "br-header",
          "br-menu",
        ],
        mergeImgHack: true,
        misMatchThreshold: 0.15,
      },
      {
        label: `GOVBR - DESENVOLVEDOR - ${page}`,
        url: `${options.baseUrl}/${page}/?tab=desenvolvedor`,
        referenceUrl: `${options.baseUrl}/${page}/?tab=desenvolvedor`,
        delay: 2000,
        removeSelectors: [
          ".is-loading[md][label], .loading[md][label], [loading][md][label]",
          ".is-loading[label], .loading[label], [loading][label]",
          "img.rounded",
          "img",
          ".scroll-to-top.show-scrollTop .br-button",
          "#barra-brasil",
          ".access-button",
          "div.show-scrollTop button.br-button",
          ".background-color-primary-default",
          "br-header",
          "br-menu",
        ],
        misMatchThreshold: 0.2,
      }
    );
  });

  pages.forEach((page) => {
    all.push({
      label: `GOVBR - PAGES - ${page}`,
      url: `${options.baseUrl}/${page}`,
      referenceUrl: `${options.baseUrl}/${page}`,
      delay: 3000,
      removeSelectors: [
        "#barra-brasil",
        ".access-button",
        ".background-color-primary-default",
        "br-header",
        "br-menu",
      ],
      mergeImgHack: true,
      misMatchThreshold: 0.15,
    });
  });

  return {
    scenarios: all,
  };
};
