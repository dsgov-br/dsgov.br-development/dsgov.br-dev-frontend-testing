# GOVBR-DS - Frontend Testing

## Informações

Esse projeto tem como objetivo realizar testes no Design System e está configurado para ser trabalhado em paralelo com o projeto GOVBR-DS.

## Clonar repositório

Clone o repositório em uma pasta:

   ```git
   git clone https://gitlab.com/govbr-ds/dev/govbr-ds-dev-frontend-testing.git
   ```

---

## BackstopJS

### Instalação

1. Com o repositório devidamente clonado, entre na pasta **govbr-ds-dev-frontend-testing**:

   ```terminal
   cd govbr-ds-dev-frontend-testing
   ```

1. Execute o comando:

   ```terminal
   npm install
   ```

### Execução

Para executar os testes (comparação com as imagens de referência), o seguinte comando deve ser executado no terminal pelo desenvolvedor depois de fazer alterações CSS quantas vezes forem necessárias:

```terminal
npm run local-test
```

Para testes relacionados a **um componente específico**, deve-se inserir o nome do componente ao final do comando:

```terminal
npm run local-test <nome-do-componente>
```

> Maiores informações relacionadas a testes no Backstopjs, tais como criação/manutenção de cenários de testes, aprovação de mudanças, comparação de imagens entre ambientes, podem ser consultadas em:
> [WIKI GOVBR-DS](https://gov.br/ds/wiki/desenvolvimento/testes/backstopjs/1-introducao/)

---

## Cypress

### Instalação

Como instalar o Cypress localmente como uma dependência do desenvolvedor ao projeto:

1. Acesse a pasta do projeto *govbr-ds-dev-frontend-testing*
2. Dentro do diretório instale o cypress

```terminal
npm install cypress --save-dev
```

### Execução

>**LEMBRE-SE:** se estiver trabalhando remotamente, você deve estar conectado na OpenVPN Serpro.

Após a instalação, execute o cypress através de um dos comandos abaixo:

```terminal
node_modules/.bin/cypress open
```

ou

```terminal
npx cypress open
```

Para executar o cypress no terminal:

```terminal
cypress run
```

---

## Wiki de Testes

Para mais detalhes, consulte a documentação do Backstopjs e/ou Cypress disponíveis na Wiki:

[WIKI GOVBR-DS](https://gov.br/ds/wiki/desenvolvimento/testes)

**LEMBRE-SE:** se estiver trabalhando remotamente, você deve estar conectado na OpenVPN Serpro.

> Os roteiros devem ser seguidos para os Sistemas Operacionais **Ubuntu** e **Windows**.
> Usuários de **Windows com WSL** devem aguardar o roteiro específico que está sendo gerado.

---

## Versões Utilizadas neste Projeto

- Backstopjs: 5.3.2
- Cypress: 6.1.0
- cypress-axe: 0.12.0
