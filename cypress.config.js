const { defineConfig } = require('cypress')
const  fs = require('fs')
const { lighthouse, prepareAudit } = require('@cypress-audit/lighthouse')

module.exports = defineConfig({
  viewportWidth: 1024,
  viewportHeight: 768,
  defaultCommandTimeout: 10000,
  video: false,
  chromeWebSecurity: true,
  e2e: {
    baseUrl: 'https://docs-ds.estaleiro.serpro.gov.br/docs/ds/dist/',

    setupNodeEvents(on, config) {
      on("before:browser:launch", (browser = {}, launchOptions) => {
        prepareAudit(launchOptions);
      });

      on("task", {
        lighthouse:lighthouse((lighthouseReport) => {
          const reportHtml = lighthouseReport.report;
          fs.writeFileSync('lhreport.html', reportHtml);
        }),
      });
  }
  },
})
