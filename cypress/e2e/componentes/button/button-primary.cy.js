///<reference types="Cypress" />

const dsComponenteUrl = "components/button/examples.html";
let componentClass = ".br-button";
const fileData = "button.json";

let tokens;

describe("VALIDATE COMPONENT BUTTON - PRIMARY", () => {

    beforeEach(() => {
        cy.visit(dsComponenteUrl);
        cy.fixture(fileData).then((data) => (tokens = data));
    });
    describe("botão retangular", () => {
        it("Validate design tokens", () => {
            cy.get(componentClass)
                .filter('.primary')
                .not('.circle')
                .then(($button) => {
                    cy.checkDesign($button, tokens.primary); //REAPROVEITAMENTO DE CÓDIGO
                });
        });

        it("Validate border radius", () => {
            cy.get(componentClass)
                .filter('.primary')
                .not('.circle')
                .should("have.css", "border-radius")
                .and("eq", tokens.rectangular.borderRadius);
        });

        it('Validate button design on focus', () => {
            cy.get(componentClass)
                .filter('.primary')
                .not('.circle')
                .not('.loading')
                .click({ force: true, multiple: true }).then(($btn) => {
                    cy.focus($btn, tokens.focus); //REAPROVEITAMENTO DE CÓDIGO
                })
        })
/*
        it('Validate background image on focus', () => {
            cy.get(componentClass)
                .filter('.primary')
                .not('.circle')
                .not('.loading')
                .click({ force: true, multiple: true }).then(($btn) => {
                    cy.get($btn).should('have.css', 'background-image')
                    .and('eq', tokens.focus.backgroundImageOnFocus)
                })
        })
*/
    });
    describe("botão circular", () => {
        it("Validate design tokens", () => {
            cy.get(componentClass)
                .filter('.primary')
                .filter('.circle')
                .then(($button) => {
                    cy.checkDesign($button, tokens.primary); //REAPROVEITAMENTO DE CÓDIGO
                });
        });

        it("Validate border radius", () => {
            cy.get(componentClass)
                .filter('.primary')
                .filter('.circle')
                .should("have.css", "border-radius")
                .and("eq", tokens.circle.borderRadius);
        });

        it('Validate button design on focus', () => {
            cy.get(componentClass)
                .filter('.primary')
                .filter('.circle')
                .not('.loading')
                .click({ force: true, multiple: true }).then(($btn) => {
                    cy.focus($btn, tokens.focus); //REAPROVEITAMENTO DE CÓDIGO
                })
        })

    })
});
