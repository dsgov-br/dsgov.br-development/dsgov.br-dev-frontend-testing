///<reference types="Cypress" />

const dsComponenteUrl = "components/button/examples.html";
let componentClass = ".br-button";
const fileData = "button.json";

let tokens;

describe("VALIDATE COMPONENT BUTTON - DISABLED", () => {

    beforeEach(() => {
        cy.visit(dsComponenteUrl);
        cy.fixture(fileData).then((data) => (tokens = data));
    });
    it("Validate disabled status - not-allowed", () => {
        cy.get("[disabled]")
            .trigger("mouseover", {
                force: true
            })
            .should("have.css", "cursor")
            .and("eq", "not-allowed");
    });

    it("Validate disabled status - opacity", () => {
        cy.get("[disabled]")
            .should("have.css", "opacity").and("eq", tokens.disabled.opacity);
    });
})