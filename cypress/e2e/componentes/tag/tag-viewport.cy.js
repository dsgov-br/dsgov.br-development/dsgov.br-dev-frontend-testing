///<reference types="Cypress" />

const dsComponenteUrl = 'components/tag/examples.html'
const componentClass = '.br-tag'
const fileData = 'tag.json'

let tokens

describe('Viewport tests', () => {

    //TESTE EM VÁRIOS VIEWPORTS
    const sizes = ['iphone-6', 'ipad-2', [1024, 768]]

    beforeEach(() => {
        cy.visit(dsComponenteUrl)
        cy.fixture(fileData).then((data) => (tokens = data))
    })

    sizes.forEach((size) => {
        it(`Should display the component correctly on ${size} screen`, () => {
            if (Cypress._.isArray(size)) {
                cy.viewport(size[0], size[1])
            } else {
                cy.viewport(size)
            }

            cy.get(componentClass).should('be.visible')
            cy.checkDesign(componentClass, tokens)
        })
    })
})