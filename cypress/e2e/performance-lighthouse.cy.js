describe("Lighhouse integration with cypress", () => {
    // const url = "https://www.gov.br/ds/";
    const url = "https://govbr-ds.gitlab.io/prototipos/prototipo-site-v4/";

    const thresholds = {
        "accessibility": 90,
        "best-practices": 50,
        "seo": 50,
        "pwa": 20,
      }

      const lighthouseOptions = {
        formFactor: 'desktop',
        screenEmulation: { disabled: true },
      }

      const lighthouseConfig = {
        settings: { output: "html" },
        extends: "lighthouse:default",
      }

    beforeEach("visit website", () => {
      cy.visit(url);
    });

    it("generates Lighthouse report", () => {

        cy.lighthouse(thresholds, lighthouseOptions, lighthouseConfig)

    });

    // it('generates Lighthouse report', () => {
    //   cy.task('lighthouse', { url }).then((report) => {
    //     const htmlReport = report.html;
    // // Save the HTML report to a file
    //     cy.writeFile('lighthouse-report.html', htmlReport);
    //     // Use the HTML report for further analysis or sharing with stakeholders
    //   });
    // });



  });