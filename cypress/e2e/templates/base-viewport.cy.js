///<reference types="Cypress" />

const dsComponenteUrl = '/templates/base/examples/base-offcanvas.html';
const componentClass = '.br-header'

//TESTE EM VÁRIOS VIEWPORTS
const sizes = ['iphone-6', 'ipad-2', [1024, 768]]

describe('Viewport tests', () => {

    beforeEach(() => {
        cy.visit(dsComponenteUrl)
    })

    sizes.forEach((size) => {
        it(`Should display the component correctly on ${size} screen`, () => {
            if (Cypress._.isArray(size)) {
                cy.viewport(size[0], size[1])
            } else {
                cy.viewport(size)
            }

            cy.get(componentClass).should('be.visible')
        })
    })
})