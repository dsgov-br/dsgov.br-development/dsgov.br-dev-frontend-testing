/// <reference types="Cypress" />
const request = require('request');
const url = Cypress.config('baseUrl');
const dsComponenteUrl = 'components/tag/examples.html';


describe('Validate acessibility a11y', () => {

 let array1 = [
 'components/accordion/examples.html',
 'components/avatar/examples.html',
 'components/breadcrumb/examples.html',
 'components/button/examples.html',
 'components/card/examples.html',
 'components/checkbox/examples.html',
 'components/datetimepicker/examples.html',
 'components/divider/examples.html',
 'components/footer/examples.html',
 'components/header/examples.html',
 'components/input/examples.html',
 'components/list/examples.html',
 'components/loading/examples.html',
 'components/menu/examples.html',
 'components/message/examples.html',
 'components/modal/examples.html',
 'components/notification/examples.html',
 'components/pagination/examples.html',
//  'components/radiobutton/examples.html',
 'components/scrim/examples.html',
 'components/select/examples.html',
 'components/table/examples.html',
 'components/tab/examples.html',
 'components/tag/examples.html',
 'components/textarea/examples.html',
 'components/tooltip/examples.html',
 'components/upload/examples.html',
 'components/wizard/examples.html' ];


let options = {json: true};


 array1.forEach(page => {
    it('Validando Acessiblidade do '+page, () => {
        cy.visit(page)

        cy.injectAxe()
        cy.configureAxe({
           rules: [
                {
                    id: 'landmark-one-main',
                    enabled: false
                },
                {
                    id: 'page-has-heading-one',
                    enabled: false
                },
                {
                  id: 'region',
                  enabled: false
                }
            ]
        })
        cy.checkA11y({exclude: ['.br-tag-input']})
    })
 })
})

