// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import "cypress-web-vitals";
import '@cypress-audit/lighthouse/commands'

//Solução para cores em hexadecimal
//https://github.com/cypress-io/cypress/issues/2186
const compareColor = (color, property) => (targetElement) => {
    const tempElement = document.createElement('div');
    tempElement.style.color = color;
    tempElement.style.display = 'none'; // make sure it doesn't actually render
    document.body.appendChild(tempElement); // append so that `getComputedStyle` actually works

    const tempColor = getComputedStyle(tempElement).color;
    const targetColor = getComputedStyle(targetElement[0])[property];

    document.body.removeChild(tempElement); // remove it because we're done with it

    expect(tempColor).to.equal(targetColor);
};

Cypress.Commands.overwrite('should', (originalFn, subject, expectation, ...args) => {
    const customMatchers = {
        'have.backgroundColor': compareColor(args[0], 'backgroundColor'),
        'have.color': compareColor(args[0], 'color'),
    };

    // See if the expectation is a string and if it is a member of Jest's expect
    if (typeof expectation === 'string' && customMatchers[expectation]) {
        return originalFn(subject, customMatchers[expectation]);
    }
    return originalFn(subject, expectation, ...args);
});

// REUSO DE COMANDOS PARA CHECAR O DESIGN DE COMPONENTES
Cypress.Commands.add("checkDesign", (componentClass, tokens) => {
    if (tokens.backgroundColor)
        cy.get(componentClass).should(
            "have.backgroundColor",
            tokens.backgroundColor
        );

    if (tokens.border)
        cy.get(componentClass)
        .should("have.css", "border")
        .and("eq", tokens.border);

    if (tokens.borderRadius)
        cy.get(componentClass)
        .should("have.css", "border-radius")
        .and("eq", tokens.borderRadius);

    if (tokens.fontColor)
        cy.get(componentClass).should("have.color", tokens.fontColor);

    if (tokens.fontWeight)
        cy.get(componentClass)
        .should("have.css", "font-weight")
        .and("eq", tokens.fontWeight);

    if (tokens.fontSize)
        cy.get(componentClass)
        .should("have.css", "font-size")
        .and("eq", tokens.fontSize);

    if (tokens.fontFamily)
        cy.get(componentClass)
        .should("have.css", "font-family")
        .and("eq", tokens.fontFamily);

    if (tokens.height)
        cy.get(componentClass)
        .should("have.css", "height")
        .and("eq", tokens.height);

});

// REUSO DE COMANDOS PARA CHECAR O COMPONENTES DESABILITADOS
Cypress.Commands.add("disabled", (tokens) => {
    if (tokens.disabled) {
        cy.get("[disabled]")
            .should("have.css", "opacity").and("eq", tokens.opacity);
    } else {
        cy.get("[disabled]")
            .should("have.backgroundColor", "#ededed")
            .should("have.color", "#ccc");
    }

    cy.get("[disabled]")
        .trigger("mouseover")
        .should("have.css", "cursor")
        .and("eq", "not-allowed");
});

// REUSO DE COMANDOS PARA CHECAR O FOCUS DO COMPONENTE
// Cypress.Commands.add("focus", (componentClass, tokens) => {
//     if (tokens.fontColorOnFocus)
//         cy.get(componentClass)
//         .should("have.color", tokens.fontColorOnFocus)

//     if (tokens.backgroundColorOnFocus)
//         cy.get(componentClass)
//         .should("have.backgroundColor", tokens.backgroundColorOnFocus)

//     if (tokens.backgroundImageOnFocus)
//     cy.get(componentClass)
//     .should('have.css', 'background-image')
//     .and('eq', tokens.backgroundImageOnFocus)

// });